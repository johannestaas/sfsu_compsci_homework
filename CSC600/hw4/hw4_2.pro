% Johan Nestaas
% CSC600 hw4 part 2

member(X,[X|T]).
member(X,[H|T]) :-
member(X,T).

first(X,[X|T]).

last(X,[X]).
last(X,[H|T]) :-
last(X, T).

adj2(X, Y, [X,Y | T]).
adj2(X, Y, [H | T]) :-
adj2(X, Y, T).

adj3(X, Y, Z, [X,Y,Z | T]).
adj3(X, Y, Z, [H | T]) :-
adj3(X, Y, Z, T).

% this order: append([1,2,3],[1],[2,3])
append(LIST1, [], LIST2) :- LIST1 = LIST2.
append(LIST1, LIST2, []) :- LIST1 = LIST2.
append([H3 | T3], [H2 | T2], LIST1) :-
H3 = H2,
append(T3, T2, LIST1).

deleteAt(T, [H|T], 0).
deleteAt([RESULTH|RESULTT], [H|T], I) :-
RESULTH = H,
I2 is I-1,
deleteAt(RESULTT, T, I2).

appendElement(LIST1, LIST2, ELE) :-
append(LIST1, LIST2, [ELE]).

insertAt([H|T], T, H, 0).
insertAt([RH|RT], [H|T], E, I) :-
RH=H,
I2 is I-1,
insertAt(RT, T, E, I2).

len([], 0).
len([H|T], L) :-
len(T, L2),
L is L2+1.

rev([], []).
rev([H|T], L) :-
rev(T, L2),
appendElement(L, L2, H).

palindrome([]).
palindrome([H]).
palindrome([X|Y]) :-
last(LAST,Y), !,
X=LAST, !,
appendElement(Y,Y2,X), !,
palindrome(Y2), !.

display([]) :- nl.
display([H|T]) :- write(H), tab(1), display(T), !.
