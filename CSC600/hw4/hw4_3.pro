% Johan Nestaas
% CSC600 hw4 part 3
% 8 queens

member(X, [X|T]).
member(X, [H|T]) :-
member(X, T).

good(NUM) :- member(NUM,[1,2,3,4,5,6,7,8]).

safe(_, []).
safe(X/Y, [XH/YH | T]) :-
X =\= XH,
Y =\= YH,
abs(X-XH) =\= abs(Y-YH),
safe(X/Y, T).

legal([]).
legal([X/Y | T]) :-
legal(T),
good(X),
good(Y),
safe(X/Y, T).


q8(X) :-
X = [1/_, 2/_, 3/_, 4/_, 5/_, 6/_, 7/_, 8/_],
legal(X).

drawRow(X) :-
write('|'),tab(X-1),write('Q'),tab(8-X),write('|'),nl, !.

board :-
q8(X),
Z=X,
Z=[1/X1, 2/X2, 3/X3, 4/X4, 5/X5, 6/X6, 7/X7, 8/X8],
write(' ----------'), nl,
write('1'), drawRow(X1),
write('2'), drawRow(X2),
write('3'), drawRow(X3),
write('4'), drawRow(X4),
write('5'), drawRow(X5),
write('6'), drawRow(X6),
write('7'), drawRow(X7),
write('8'), drawRow(X8),
write(' ----------'), nl,
write('  12345678 '), nl.

