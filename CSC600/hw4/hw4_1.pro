% Johan Nestaas
% CSC600 hw4 part 1

m([johan, mason, tommy, nolan, ethan, mark, eirik, josh, grandpa, rikoll, james, vince, tyrone, ancientjohan]).
f([loraine, sharon, marian, toni, grete, grace, kendra, jan, anne, notsure]).

family([grandpa, loraine, [sharon, marian, mark]]).
family([rikoll, toni, [eirik, grete]]).
family([eirik, sharon, [johan]]).
family([james, sharon, [mason, tommy]]).
family([mason, grace, [nolan]]).
family([tommy, kendra, [ethan]]).
family([vince, jan, [grace, josh, anne]]).
family([tyrone, anne, [nikita]]).
family([ancientjohan, notsure, [rikoll]]).

member(X, [X|T]).
member(X, [H|T]) :- member(X, T).

male(X) :- m(Y), !, member(X, Y).
female(X) :- f(Y), !, member(X, Y).

father(DAD,KID) :-
family([DAD, MOM, LIST]),
member(KID, LIST).

mother(MOM,KID) :-
family([DAD, MOM, LIST]),
member(KID, LIST).

parent(PARENT, KID) :-
father(PARENT, KID).
parent(PARENT, KID) :-
mother(PARENT, KID).

siblings1(BRO1, BRO2) :-
father(PARENT, BRO1), 
father(PARENT, BRO2),
BRO1 @< BRO2,
BRO1 \= BRO2.

siblings1(BRO1, BRO2) :-
mother(PARENT, BRO1), 
mother(PARENT, BRO2),
BRO1 @< BRO2,
BRO1 \= BRO2.

siblings2(BRO1, BRO2) :-
family([DAD,MOM,KIDS]),
member(BRO1, KIDS),
member(BRO2, KIDS),
BRO1 @< BRO2.

brother1(BRO1, OTHER) :-
siblings1(BRO1, OTHER),
male(BRO1).
brother2(BRO1, OTHER) :-
siblings2(BRO1, OTHER),
male(BRO1).
sister1(SIS1, OTHER) :-
siblings1(SIS1, OTHER),
female(SIS1).
sister2(SIS1, OTHER) :-
siblings2(SIS1, OTHER),
female(SIS1).

cousins(CUZ1, CUZ2) :-
parent(DAD1, CUZ1),
parent(DAD2, CUZ2),
siblings1(DAD1, DAD2).

uncle(UNCLE,NEPHEW) :-
parent(PARENT, NEPHEW),
brother1(UNCLE, PARENT).

aunt(AUNT,NEPHEW) :-
parent(PARENT, NEPHEW),
sister1(AUNT, PARENT).

grandchild(GC, GP) :-
parent(PARENT, GC),
parent(GP, PARENT).

grandson(GC, GP) :-
grandchild(GC, GP),
male(GC).

granddaughter(GC, GP) :-
grandchild(GC, GP),
female(GC).

greatgrandparent(GGP, GGC) :-
grandchild(GGC, GP),
parent(GGP, GP).

ancestor(ANCESTOR, PERSON) :-
parent(ANCESTOR, PERSON).
ancestor(ANCESTOR, PERSON) :-
parent(PARENT, PERSON),
ancestor(ANCESTOR, PARENT).

