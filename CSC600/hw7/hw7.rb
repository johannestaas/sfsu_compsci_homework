#!/usr/bin/env ruby

def rand_array()
    arr = []
    for i in 0...100
        arr[i] = rand(11)
    end
    return arr
end

def show(v)
    for i in v
        print("#{i}\t")
    end
    puts
end

def hist(v)
    h = {}
    for i in v
        if h.has_key?(i)
            h[i]+=1
        else
            h[i]=1
        end
    end
    for i in 0..10
        print "#{i}\t"
        for j in 1..h[i]
            print "*"
        end
        puts
    end
end

def mean(v)
    m = 0.0
    for i in v
        m += i
    end
    return m / v.length
end

def sigma(v)
    m = mean(v)
    s = 0.0
    for i in v
        s += (i - m)**2
    end
    s /= v.length-1
    return Math::sqrt(s)
end

def mean_sigma(v)
    return mean(v), sigma(v)
end

def merge(left, right)
    r = []
    while left.length > 0 or right.length > 0
        if left.length > 0 and right.length > 0
            if left[0] <= right[0]
                r += [left[0]]
                left = left[1...left.length]
            else
                r += [right[0]]
                right = right[1...right.length]
            end
        elsif left.length > 0
            return r + left
        elsif right.length > 0
            return r + right
        end
    end
    return nil
end

def sort(v)
    if v.length <= 1
        return v
    end
    mid = v.length / 2
    left = sort(v[0...mid])
    right = sort(v[mid...v.length])
    return merge(left, right)
end

def binary_search(v, x, *n)
    # i'm assuming v is already sorted, as HW says
    # n is the rest of the args, which will be start of the array
    if n.empty?
        n = 0
    else
        n = n[0]
    end
    if v.length <= 1
        if v[0] == x
            return n
        else
            return -1
        end
    end
    mid = v.length/2
    if x < v[mid]
        return binary_search(v[0...mid], x, n)
    elsif x == v[mid]
        return mid + n
    else
        return binary_search(v[mid+1...v.length], x, n+mid+1)
    end
    return nil
end

def min_max(v)
    if v.empty? 
        return nil
    end
    min = v[0]
    max = v[0]
    for i in v[0...v.length]
        if i < min
            min = i
        end
        if i > max
            max = i
        end
    end
    return min,max
end

puts "1. array of ints (look at next)"
puts
a = rand_array()
puts "2. show array"
show(a)
puts
puts "3. histogram"
hist(a)
puts
puts "4. mean"
puts("Mean: ", mean(a))
puts
puts "5. sample std dev"
puts("Sample Std Dev: ", sigma(a))
puts
puts "6. mean and sigma"
puts("mean_sigma:\t", mean_sigma(a))
puts
puts "7. sort it (implemented mergesort)"
show(sort(a))
puts
puts "8. binary search (used a new array 0...1000)"
# i'm making an array of 1000 unique digits 0->999 for this, since the rand has
# dupes
bs_arr = []
for i in 0...1000
    bs_arr += [i]
end
puts "40 is in index ", binary_search(bs_arr, 40)
puts "811 is in index ", binary_search(bs_arr, 811)
puts "1000 is in index ", binary_search(bs_arr, 1000)
puts
puts "9. display min and max of array (using old 100 0-10 array)"
puts "min and max: ", min_max(a)
puts
puts "10."
puts "already showed everything!"

