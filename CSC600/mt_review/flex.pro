true(brother, joe, jeff).
true(brother, jeff, jack).
true(brother, joe, jack).

transitive(REL, A, B, C) :-
true(REL, A, B),
true(REL, B, C),
true(REL, A, C),
A \= B,
B \= C,
A \= C.

inc(X, Y) :-
Y is X+1.

