
age(joe, 20).
age(max, 12).
age(kevin, 30).
age(cindy, 18).
age(lisa, 40).

parent(dad1, joe).
parent(dad1, max).
parent(dad2, kevin).
parent(dad2, cindy).
parent(dad2, lisa).

parent(granddad, dad1).
parent(granddad, dad2).

grandchild(GC, GP) :-
parent(PARENT, GC),
parent(GP, PARENT).

older(X, Y, X) :-
age(X, A1),
age(Y, A2),
A1 >= A2, !.

older(X, Y, Y) :-
age(X, A1),
age(Y, A2),
A2 > A1, !.

oldest([H], H).
oldest([H|T], H) :-
oldest(T, O),
older(H, O, H).
oldest([H|T], O) :-
oldest(T, O),
older(H, O, O).

oldest_gc(GP, OLDEST) :-
findall(X, grandchild(X, GP), L),
oldest(L, OLDEST).

