
min([H], H).
min([H|T], H) :-
min(T, M),
H<M.
min([H|T], M) :-
min(T, M),
M =< H.

max([H], H).
max([H|T], H) :-
max(T, M),
H>M.
max([H|T], M) :-
max(T, M),
M >= H.


sum([], 0).

sum(L, M) :-
min(L, MIN), !,
max(L, MAX), !,
M is MIN + MAX, !.

