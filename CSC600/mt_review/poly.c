#include <stdlib.h>
#include <stdio.h>

// new_poly(struct poly* p, float* p_head, int len)
#define new_poly(p, p_head, len2) \
    { int new_poly_i; \
    p_head = malloc(len2*sizeof(float)); \
    p = malloc(sizeof(struct poly)); \
    p->head = p_head; \
    p->len = len2; \
    for(new_poly_i=0;new_poly_i<len2;new_poly_i++) p->head[new_poly_i]=0.f; }

// head[i] is the coefficient c of c*x^i
struct poly {
    float* head;
    int len;
};

void p_kill(struct poly *p)
{
    int i;
    free(p->head);
    free(p);
}

void p_print(struct poly *p)
{
    int i,last;
    last = -1;
    for(i=0; i<p->len; i++)
        if(p->head[i])
            last = i;

    if(p->head[0] != 0)
    {
        printf("%.2f", p->head[0]);
        if(last>1) printf(" + ");
    }

    for(i=1; i<p->len; i++)
    {
        if(p->head[i] == 0) continue;
        printf("%.2f*x^%d", p->head[i], i);
        if(i<last) printf(" + ");
    }
    printf("\n");
}

struct poly* p_add(struct poly *p1, struct poly *p2)
{
    struct poly* p;
    float* p_head;
    int maxlen;
    int i;
    maxlen = p1->len > p2->len ? p1->len : p2->len;
    new_poly(p, p_head, maxlen);
    for(i=0;i<maxlen;i++)
    {
        float c1, c2;
        if(i > p1->len)
            c1 = 0; 
        else c1 = p1->head[i];
        if(i > p2->len) 
            c2 = 0; 
        else c2 = p2->head[i];
        *p_head = c1 + c2;
        p_head++;
    }
    return p;
}

struct poly* p_mul(struct poly *p1, struct poly *p2)
{
    struct poly* p;
    float* p_head;
    int len;
    int i,j;
    len = p1->len + p2->len;
    new_poly(p, p_head, len);
    for(i=0; i<p1->len; i++)
        for(j=0; j<p2->len; j++)
            p->head[i+j] += p1->head[i]*p2->head[j];
    return p;
}

struct poly* p_dx(struct poly *p1)
{
    struct poly* p;
    float* p_head;
    int len;
    int i;
    len = p1->len - 1;
    new_poly(p, p_head, len);
    for(i=0; i<len; i++)
        p->head[i] = p1->head[i+1]*(i+1);
    return p;
}

struct poly* p_integral(struct poly *p1)
{
    struct poly* p;
    float* p_head;
    int len;
    int i;
    len = p1->len + 1;
    new_poly(p, p_head, len);
    for(i=1; i<len; i++)
    {
        p->head[i] = p1->head[i-1]/i;
    }
    return p;
}

int main()
{
    struct poly p1, p2, *p3;
    float p1_list[5] = {1.f,2.f,1.f,1.f,4.f};
    float p2_list[6] = {2.f,0.f,3.f,0.f,2.f,1.f};
    int i;
    p1.head = p1_list;
    p1.len = 5;
    p2.head = p2_list;
    p2.len = 6;
    p_print(&p1);
    printf("+\n");
    p_print(&p2);
    printf("--------------------------------\n");
    p3 = p_add(&p1, &p2);
    p_print(p3);
    printf("\n");
    p_kill(p3);

    p_print(&p1);
    printf("*\n");
    p_print(&p2);
    printf("--------------------------------\n");
    p3 = p_mul(&p1, &p2);
    p_print(p3);
    printf("\n");
    p_kill(p3);

    printf("d/dx of ");
    p_print(&p1);
    printf("--------------------------------\n");
    p3 = p_dx(&p1);
    p_print(p3);
    printf("\n");
    p_kill(p3);

    printf("integral of ");
    p_print(&p1);
    printf("--------------------------------\n");
    p3 = p_integral(&p1);
    p_print(p3);
    printf("\n");
    p_kill(p3);
    return 0;
}

