% Johan Nestaas
% CSC600 HW3

teaches(hsu, csc210_2).
afternoon(csc210_2).
freshman(csc210_2).
mwf(csc210_2).

teaches(sosnick, csc210_3).
morning(csc210_3).
freshman(csc210_3).
tth(csc210_3).

teaches(sosnick, csc210_4).
afternoon(csc210_4).
freshman(csc210_4).
tth(csc210_4).

teaches(wong, csc220_1).
morning(csc220_1).
freshman(csc220_1).
mwf(csc220_1).

teaches(wong, csc220_2).
morning(csc220_2).
freshman(csc220_2).
mwf(csc220_2).

teaches(okada, csc230_2).
afternoon(csc230_2).
freshman(csc230_2).
tth(csc230_2).

teaches(hsu, csc256_1).
afternoon(csc256_1).
sophomore(csc256_1).
mwf(csc256_1).

teaches(jozo, csc340_1).
afternoon(csc340_1).
sophomore(csc340_1).
mwf(csc340_1).

teaches(murphy, csc415_1).
afternoon(csc415_1).
junior(csc415_1).
mwf(csc415_1).

teaches(wall, csc510_1).
morning(csc510_1).
junior(csc510_1).
tth(csc510_1).

teaches(wall, csc520_12).
afternoon(csc520_12).
junior(csc520_12).
tth(csc520_12).

teaches(jozo, csc600_1).
morning(csc600_1).
senior(csc600_1).
mwf(csc600_1).

teaches(murphy, csc675_1).
afternoon(csc675_1).
senior(csc675_1).
mwf(csc675_1).

teaches_freshman(PROF) :-
teaches(PROF, CLASS),
freshman(CLASS).

teaches_sophomore(PROF) :-
teaches(PROF, CLASS),
sophomore(CLASS).

teaches_junior(PROF) :-
teaches(PROF, CLASS),
junior(CLASS).

teaches_senior(PROF) :-
teaches(PROF, CLASS),
senior(CLASS).

teaches_morning(PROF) :-
teaches(PROF, CLASS),
morning(CLASS).

teaches_afternoon(PROF) :-
teaches(PROF, CLASS),
afternoon(CLASS).

teaches_whole_day(PROF) :-
teaches_morning(PROF),
teaches_afternoon(PROF).

works_mwf(PROF) :-
teaches(PROF, CLASS),
mwf(CLASS).

works_tth(PROF) :-
teaches(PROF, CLASS),
tth(CLASS).

teaches_three_classes(PROF) :-
teaches(PROF, CLASS1),
teaches(PROF, CLASS2),
teaches(PROF, CLASS3),
CLASS1 \= CLASS2,
CLASS2 \= CLASS3,
CLASS3 \= CLASS1.

