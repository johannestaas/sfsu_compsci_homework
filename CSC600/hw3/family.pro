% Johan Nestaas
% CSC600 hw3

is_male(mason).
is_male(tommy).
is_male(eirik).
is_male(johan).
is_male(ethan).
is_male(nolan).
is_male(erik).
is_female(grace).
is_female(kendra).
is_female(anya).
is_female(sharon).
is_female(catherine).
is_male(josh).
is_female(anne).
is_parent_of(mason, nolan).
is_parent_of(grace, nolan).
is_parent_of(tommy, ethan).
is_parent_of(kendra, ethan).
is_parent_of(eirik, johan).
is_parent_of(sharon, johan).
is_parent_of(sharon, mason).
is_parent_of(sharon, tommy).
is_parent_of(eirik, erik).
is_parent_of(eirik, anya).
is_parent_of(catherine, erik).
is_parent_of(catherine, anya).
is_parent_of(loraine, sharon).
is_parent_of(toni, eirik).
is_female(loraine).
is_female(toni).
is_female(grete).
is_parent_of(toni, grete).
is_male(rikoll).
is_parent_of(rikoll, grete).
is_parent_of(rikoll, eirik).

mother(MOTHER, CHILD) :-
is_female(MOTHER),
is_parent_of(MOTHER, CHILD).

father(FATHER, CHILD) :-
is_male(FATHER),
is_parent_of(FATHER, CHILD).

sibling1(NAME1, NAME2) :-
is_parent_of(PARENT, NAME1),
is_parent_of(PARENT, NAME2),
NAME1 \= NAME2.

brother1(NAME1, NAME2) :-
sibling1(NAME1, NAME2),
is_male(NAME1).

sister1(NAME1, NAME2) :-
sibling1(NAME1, NAME2),
is_female(NAME1).

sibling2(NAME1, NAME2) :-
is_parent_of(PARENT, NAME1),
is_parent_of(PARENT, NAME2),
is_parent_of(PARENT2, NAME1),
is_parent_of(PARENT2, NAME2),
NAME1 \= NAME2,
PARENT \= PARENT2.

brother2(NAME1, NAME2) :-
sibling2(NAME1, NAME2),
is_male(NAME1).

sister2(NAME1, NAME2) :-
sibling2(NAME1, NAME2),
is_female(NAME1).

cousin(NAME1, NAME2) :-
is_parent_of(PARENT, NAME1),
is_parent_of(PARENT2, NAME2),
sibling1(PARENT, PARENT2).

uncle(UNCLE, NAME2) :-
is_parent_of(PARENT, NAME2),
brother1(UNCLE, PARENT).

aunt(AUNT, NAME2) :-
is_parent_of(PARENT, NAME2),
sister1(AUNT, PARENT).

grandparent(GP, GC) :-
is_parent_of(PARENT, GC),
is_parent_of(GP, PARENT).

grandmother(GM, GC) :-
grandparent(GM, GC),
is_female(GM).

grandfather(GF, GC) :-
grandparent(GF, GC),
is_male(GF).

grandchild(GC, GP) :-
grandparent(GP, GC).

greatgrandparent(GGP, GGC) :-
grandparent(GP, GGC),
is_parent_of(GGP, GP).

ancestor(ANC, NAME) :-
is_parent_of(ANC, NAME);
is_parent_of(PARENT, NAME),
ancestor(ANC, PARENT).

