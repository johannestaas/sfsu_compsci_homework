// Johan Nestaas
// CSC 600
// hw2-5
// "Binary Search"
// use iterative and recursive
// check performance of each, with -O3 and -O0 as well

#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

#define ARR_SIZE 100000
#define ITER 500

int bsearch_rec(int *arr, int len, int item)
{
    int half, halflen;
    if(len == 1) return 0;
    halflen = len%2 + len/2;
    half = arr[halflen];
    if(half == item) return halflen;
    if(half > item) return bsearch_rec(arr, halflen, item);
    return bsearch_rec(arr+halflen, halflen, item) + halflen;
}

int bsearch_iter(int *arr, int len, int item)
{
    int half, halflen;
    int *head;
    if(len == 1) return 0;
    head = arr-1;
    halflen = len/2;
    while(1)
    {
        half = *(head + halflen);
        if(halflen == 1 && half != item)
            return -1;
        if(half==item)
            return head + halflen - arr;
        if(half<item)
            head = head + halflen;
        halflen = halflen/2 + halflen%2;
    }
}

int main()
{
    struct timeval start1, start2, end;
    double seconds[2], useconds[2];
    // just so this doesn't get optimized out
    int sum=0;
    int i,j;
    int arr[ARR_SIZE];
    for(i=0;i<ARR_SIZE;i++)
        arr[i] = i;
    gettimeofday(&start1, NULL);
    for(j=0;j<ITER;j++)
        for(i=0;i<ARR_SIZE;i++)
            sum+=bsearch_iter(arr, ARR_SIZE, i);
    gettimeofday(&start2, NULL);
    for(j=0;j<ITER;j++)
        for(i=0;i<ARR_SIZE;i++)
            sum+=bsearch_rec(arr, ARR_SIZE, i);
    gettimeofday(&end, NULL);
    seconds[1] = end.tv_sec - start2.tv_sec;
    seconds[0] = start2.tv_sec - start1.tv_sec;
    useconds[1] = end.tv_usec - start2.tv_usec;
    useconds[0] = start2.tv_usec - start1.tv_usec;
    useconds[0] /= 1000000;
    useconds[1] /= 1000000;
    printf("Iterative Time:\t %f\n", seconds[0] + useconds[0]);   
    printf("Recursive Time:\t %f\n", seconds[1] + useconds[1]);   
    // use sum to confuse compiler
    if(sum==0)
        return -1;
    return 0;
}

