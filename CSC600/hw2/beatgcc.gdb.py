#!/usr/bin/env python

import gdb

# for testing beatgcc.asm
gdb.execute("b 63 if i=500")
gdb.execute("r")
gdb.execute("s 3")
gdb.execute("wh")
gdb.execute("tui reg g")
gdb.execute("wh reg -8")
