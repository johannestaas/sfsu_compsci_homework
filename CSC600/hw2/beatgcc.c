// Johan Nestaas
// CSC 600
// hw2-5
// "Binary Search"
// use iterative and recursive
// check performance of each, with -O3 and -O0 as well

#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

#define ARR_SIZE 100000
#define ITER 500

extern int bsearch_asm(int *arr, int len, int item);

int bsearch_rec(int *arr, int len, int item)
{
    int half, halflen;
    if(len == 1) return 0;
    halflen = len%2 + len/2;
    half = arr[halflen];
    if(half == item) return halflen;
    if(half > item) return bsearch_rec(arr, halflen, item);
    return bsearch_rec(arr+halflen, halflen, item) + halflen;
}

int bsearch_iter(int *arr, int len, int item)
{
    int half, halflen;
    int *head;
    if(len == 1) return 0;
    head = arr-1;
    halflen = len/2;
    while(1)
    {
        half = *(head + halflen);
        if(halflen == 1 && half != item)
            return -1;
        if(half==item)
            return head + halflen - arr;
        if(half<item)
            head = head + halflen;
        halflen = halflen/2 + halflen%2;
    }
}

#define ASM_TEST 0
#define GCC_TEST 1
int main()
{
    struct timeval start, end_asm, end;
    double seconds[2], useconds[2];
    // just so this doesn't get optimized out
    int test=0;
    int i,j;
    int arr[ARR_SIZE];
    for(i=0;i<ARR_SIZE;i++)
        arr[i] = i;
    gettimeofday(&start, NULL);
    for(j=0;j<ITER;j++)
    {
        for(i=0;i<ARR_SIZE;i++) 
        {
            test = bsearch_asm(arr, ARR_SIZE, i);
            if(test != i)
                fprintf(stderr, "asm didn't work for %d\n", i);
        }
    }
    gettimeofday(&end_asm, NULL);
    for(j=0;j<ITER;j++)
    {
        for(i=0;i<ARR_SIZE;i++)
        {
            test = bsearch_rec(arr, ARR_SIZE, i);
            if(test != i)
                fprintf(stderr, "rec didn't work for %d\n", i);
        }
    }
    gettimeofday(&end, NULL);
    seconds[ASM_TEST] = end_asm.tv_sec - start.tv_sec;
    seconds[GCC_TEST] = end.tv_sec - end_asm.tv_sec;
    useconds[ASM_TEST] = end_asm.tv_usec - start.tv_usec;
    useconds[GCC_TEST] = end.tv_usec - end_asm.tv_usec;
    useconds[ASM_TEST] /= 1000000;
    useconds[GCC_TEST] /= 1000000;
    printf("Hand-Coded:\t %f sec\n", seconds[ASM_TEST] + useconds[ASM_TEST]);   
    printf("GCC Optimized:\t %f sec\n", seconds[GCC_TEST] + useconds[GCC_TEST]);
    // use test to keep compiler from cheating
    if(test==0)
        return -1;
    return 0;
}

