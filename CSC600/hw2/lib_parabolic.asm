; Johan Nestaas
; CSC 600
; hw2-2
; "Parabolic Approximation"
; approximate a curve with a parabola

; Linux x86-64 ABI
;
; no function calls inside so I'm saving time and not pushing rbp

	segment .text
	global row_div
	global row_sub

; @param rdi
;   address of row float array (length 4)
; @param xmm0
;   floating point divisor
; 
; @return void
row_div:
	shufps	xmm0,	xmm0,	0	; copies the scalar float across the 128-bit reg
	movaps	xmm1,   [rdi]		; move the WHOLE packed float array into xmm1
	divps	xmm1,	xmm0		; divide the whole row by the scalar
	movaps	[rdi],	xmm1		; copy them back to memory
	ret

; @param rdi
;	address of destination row
; @param rsi
;	address of source row
; @param rdx
;	address of scalar to multiply source row by
; 
; @return void
row_sub:
	movaps	xmm0,	[rdi]		; xmm0 = destination row
	movaps	xmm1,	[rsi]		; xmm1 = source row
	movss	xmm2,	[rdx]		; xmm2 = scalar of multiplier
	shufps	xmm2,	xmm2,	0	; fill xmm2 with multiplier scalar
	mulps	xmm1,	xmm2		; multiply source row with multiplier
	subps	xmm0,	xmm1		; packed subtract it
	movaps	[rdi],	xmm0		; write the row back to memory
	ret							; return
