// Johan Nestaas
// CSC 600
// hw2-3
// Array reduction
// Remove the sequences of the max 3 ints in an array
// this is probably a bit more complicated than it needs to be...

#include <stdio.h>
#include <stdlib.h>

struct ARR {
    int *head;
    int len;
};

// @param *arr
//  array of ints to reduce
// @param n
//  length of array
// @return n
//  new array length
struct ARR reduce(int *arr, int n)
{
    // l1 is the largest, l2 the 2nd largest, l3...
    int i, i2, l1, l2, l3, n2;
    // new array
    int *arr2;
    // return value
    struct ARR ret;
    // initialize them to the most negative number
    l1 = l2 = l3 = 0x80000000;
    // new array length, index to modify it
    n2 = i2 = 0;
    for(i=0; i<n; i++)
    {
        if((arr[i] == l1) || 
           (arr[i] == l2) || 
           (arr[i] == l3)) 
            continue;
        if(arr[i] > l1)
        {
           l3 = l2;
           l2 = l1;
           l1 = arr[i];
           continue;
        }
        if(arr[i] > l2)
        {
           l3 = l2;
           l2 = arr[i];
           continue;
        }
        if(arr[i] > l3)
           l3 = arr[i];
    }
    for(i=0; i<n; i++)
    {
        if((arr[i] == l1) || 
           (arr[i] == l2) || 
           (arr[i] == l3)) 
            continue;
        n2++;
    }
    // new array
    arr2 = malloc(n2*sizeof(int));
    for(i=0; i<n; i++)
    {
        if((arr[i] == l1) || 
           (arr[i] == l2) || 
           (arr[i] == l3)) 
            continue;
        arr2[i2++] = arr[i];
    }
    ret.head = arr2;
    ret.len = n2;
    return ret;
}

int main()
{
    int i;
    int test[] = {0,5,4,4,5,8,2,9,5,9,1,2,3,-1,-3};
    int n = 14;
    struct ARR arr;
    for(i=0; i<n; i++)
        printf("%d ", test[i]);
    printf("\n");
    arr = reduce(test, n);
    printf("%d\n", arr.len);
    for(i=0; i<arr.len; i++)
        printf("%d ", arr.head[i]);
    printf("\n");
    free(arr.head);
    return 0;
}
