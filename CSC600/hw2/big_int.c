// Johan Nestaas
// CSC 600
// hw2-4
// "Big Int"
// print big integers

#include <stdio.h>

#define b0011110 0x1E
#define b0110011 0x33
#define b0001100 0x0C
#define b0111100 0x3C
#define b0111110 0x3E
#define b0000011 0x03
#define b0000110 0x06
#define b0011000 0x18
#define b0001110 0x0E
#define b0011111 0x1F
#define b0110000 0x30
#define b0111000 0x38
#define b0000110 0x06
#define b0011100 0x1C

struct number {
    char row[7];
};

struct number font[10] = 
{
    {
        b0011110,
        b0110011,
        b0110011,
        b0110011,
        b0110011,
        b0110011,
        b0011110
    },
    {
        b0001100,
        b0111100,
        b0001100,
        b0001100,
        b0001100,
        b0001100,
        b0111110
    },
    {
        b0011110,
        b0110011,
        b0000011,
        b0000110,
        b0001100,
        b0111000,
        b0111110
    },
    {
        b0011110,
        b0000011,
        b0000011,
        b0001110,
        b0000011,
        b0000011,
        b0011110
    },
    {
        b0110011,
        b0110011,
        b0110011,
        b0011111,
        b0000011,
        b0000011,
        b0000011,
    },
    {
        b0111110,
        b0110000,
        b0110000,
        b0111100,
        b0000011,
        b0000011,
        b0111110,
    },
    {
        b0011110,
        b0110000,
        b0110000,
        b0111100,
        b0110011,
        b0110011,
        b0011110,
    },
    {
        b0111110,
        b0110011,
        b0000011,
        b0000110,
        b0001100,
        b0001100,
        b0001100,
    },
    {
        b0011110,
        b0110011,
        b0110011,
        b0011100,
        b0110011,
        b0110011,
        b0011110,
    },
    {
        b0011110,
        b0110011,
        b0110011,
        b0011111,
        b0000011,
        b0000011,
        b0000011,
    },
};

void print_row(char encoding)
{
    char c = 0x80;
    while(c)
    {
        if(c & encoding)
        {
            printf("@");
        } else {
            printf(" ");
        }
        c = (c >> 1) & 0x7f;
    }
}

void big_int(int num)
{
    int x, row, digit, rowx;
    for(row=0; row<7; row++)
    {
        rowx = num;
        for(digit=1000000000; digit>0; digit/=10)
        {
            if(num >= digit)
            {
                print_row(font[rowx/digit].row[row]);
                rowx -= rowx/digit * digit;
            }
        }
        printf("\n");
    }
    printf("\n");
}

void main()
{
    big_int(1010);
    big_int(31337);
    big_int(123456789);
}
