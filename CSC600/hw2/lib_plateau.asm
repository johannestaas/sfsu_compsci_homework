; Johan Nestaas
; CSC 600
; hw2-1
; "Plateau Program"
; Returns the length of the longest sequence of bytes

    segment .text
    global plateau

; Linux x86-64 ABI
;
; @param rdi
;   address of array
;
; @return rax
;   plateau of longest sequence of bytes

plateau:
    push    rbp
    mov     rbp,    rsp
    mov     dl,     [rdi]       ; last byte seen(dl) = first array index
    mov     rax,    1           ; plateau(rax) = 1
    mov     r9,     1           ; current string length(r9) = 1
.loop:
    inc     rdi                 ; increment char*
    mov     r8b,    [rdi]       ; load byte into low r8
    cmp     r8b,    dl          ; check if a[i] == a[i-1]
    jne     .reset              ; if not, reset and keep going
    inc     r9                  ; current str len ++
    jmp     .loop               ; keep checking
.reset:
    cmp     r9,     rax         ; check if current str len > saved value or rax
    cmovg   rax,    r9          ; if so, set return value to it
    cmp     r8b,    0           ; check if null terminated
    jz      .done               ; return rax
    mov     dl,     r8b         ; set last byte seen to a[i]
    mov     r9,     1           ; only saw one so far
    jmp     .loop               ; keep checking, will exit when saw all indices
.done:
    pop     rbp                 ; restore rbp. rbx + r12-r15 were not used
    ret                         ; return rax

