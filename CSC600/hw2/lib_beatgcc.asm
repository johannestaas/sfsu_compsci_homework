; Johan Nestaas
; CSC 600
; hw2-5
; "Recursive binary search"
; My attempt to beat the compiler. 

; Linux x86-64 ABI
;

	segment .text
	global bsearch_asm

; @param rdi
;   address of array to look through
; @param rsi
;   length of array
; @param rdx
;   item to search for
; 
; @return index of item
bsearch_asm:
	push	rbp
	mov		rbp,	rsp
	mov		rcx,	rdx				; save item to search for
.bsearch:
	cmp		rsi,	1				; if(len == 1)
	jz		.done0					;	return 0
	push	r10						; preserve r10 (old halflen)
	mov		rax,	rsi				; rax = len/2 ; rdx = len%2
	xor		rdx,	rdx				
	mov		r10,	2
	div		r10
	add		rax,	rdx				; rax = len/2 + len%2
	mov		r10,	rax				; r10 = halflen
									; rdi = &arr[0]
									; rax = index of arr
	mov		r9d,	[rax*4+rdi] 	; half = r9 = arr[halflen]
	cmp		ecx,	r9d				; if(half == item)
	jz		.done					;	return halflen
	jg		.rec_gt					; if(half > item) -> recursion
									; if(half < item) continued
	mov		rsi,	rax				; rsi = halflen
	mov		rdx,	rcx				; rdx = item
	call	bsearch_asm				; recursively find it
	pop		r10						; restore r10 (last funcs halflen)
	pop		rbp						; return bsearch + halflen
	ret								;
.rec_gt								; return bsearch_rec(arr, halflen, item);
	mov		rsi,	rax				; rsi = halflen
	imul	rax,	4				; rax = arr = arr + halflen(dword)
	add		rdi,	rax				;
	mov		rdx,	rcx				; rdx = item
	call	bsearch_asm				; recursively find it and done
	add		rax,	r10				; add old halflen to that bsearch
.done:
	pop		r10						; restore r10 (last funcs halflen)
	pop		rbp
	ret
.done0:
	xor		rax,	rax				; return 0 for current spot
	pop		rbp
	ret

