// Johan Nestaas
// CSC 600
// hw2-2
// "Parabolic Approximation"
// approximate a curve with a parabola

#include <stdio.h>

// 4 len float array, divisor
extern void row_div(float*, float);
// dest_row, src_row, multipliers address
extern void row_sub(float*, float*, float*);

// for debugging
void print_matrix(float* row1, float* row2, float* row3)
{
    printf("%f\t%f\t%f\t.\t%f\n", row1[0], row1[1], row1[2], row1[3]);
    printf("%f\t%f\t%f\t.\t%f\n", row2[0], row2[1], row2[2], row2[3]);
    printf("%f\t%f\t%f\t.\t%f\n\n", row3[0], row3[1], row3[2], row3[3]);
}

// modifies the float fx[3]
//  a,b,c for f(x) = ax2 + bx + c
void parabolic_matrix(float x1, float y1, 
                      float x2, float y2, 
                      float x3, float y3,
                      float fx[3])
{
    float row1[4] = {x1*x1, x1, 1.f, y1};
    float row2[4] = {x2*x2, x2, 1.f, y2};
    float row3[4] = {x3*x3, x3, 1.f, y3};
    row_div(row1, row1[0]);
    row_sub(row2, row1, &row2[0]);
    row_sub(row3, row1, &row3[0]);
    row_div(row2, row2[1]);
    row_sub(row3, row2, &row3[1]);
    row_sub(row1, row2, &row1[1]);
    row_div(row3, row3[2]);
    row_sub(row1, row3, &row1[2]);
    row_sub(row2, row3, &row2[2]);
    fx[0] = row1[3];
    fx[1] = row2[3];
    fx[2] = row3[3];
}

// @param fx
//      {a, b, c} for a,b,c in f(x) = a*x2 + b*x + c
// @param x
//      for calculating it
// @return
//      calculate the f(x) of that function fx at x
float parabolic_approx(float fx[3], float x)
{
    return x*x*fx[0] + x*fx[1] + fx[2];
}

// The function requested in the assignment.
// Not used since I didn't want to recalculate a,b,c for
// each y in the range between x1 and x3, but this works.
float approx(float x1, float y1, 
            float x2, float y2, 
            float x3, float y3,
            float x)
{
    float fx[3];
    parabolic_matrix(x1, y1, x2, y2, x3, y3, fx);
    return parabolic_approx(fx, x);
}

int main(int argc, char *argv[])
{
    float fx[3], i;
    if(argc != 1)
    {
        fprintf(stderr, "No arguments please.\n");
        return 1;
    }

    // stores a,b,c in fx
    // these values are for f(x) = 3x2 + 4x + 5
    parabolic_matrix(2.f, 25.f, 4.f, 69.f, 100.f, 30405.f, fx);
    printf("approximated f(x) = %f*x2 + %f*x + %f\n\n", fx[0], fx[1], fx[2]);
    
    // x1=2 x3=100
    // calculating for 5 through 45
    for(i=5.f; i<45.f; i++)
        printf("f(%f) = %f\n", i, parabolic_approx(fx, i));

    return 0;
}
