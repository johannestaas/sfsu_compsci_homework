
extern void row_div_gcc(float* row, float div)
{
    int x;
    for(x=0; x<4; x++)
        row[x] /= div;
}

extern void row_sub_gcc(float* dest, float* src, float* factor)
{
    int x;
    for(x=0; x<4; x++)
        dest[x] = dest[x] - src[x]*(*factor);
}

