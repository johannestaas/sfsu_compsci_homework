// Johan Nestaas
// CSC 600
// hw2-1
// "Plateau Program"
// Returns the length of the longest sequence of ints

#include <stdio.h>

extern int plateau(char*);

int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        fprintf(stderr, "Usage: plateau STRING\n");
        return 1;
    }
    printf("%d\n", plateau(argv[1]));
    return 0;
}
