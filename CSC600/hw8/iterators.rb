
arr = [2,4,6,8,10,12,14]

puts 'for i in arr'
for i in arr
    print "#{i}\t"
end

puts
puts 'for i in 1..10'
for i in 1..10
    print "#{i}\t"
end

puts
puts 'arr.each'
arr.each {|i| print "#{i}\t"}

puts
puts 'arr.length.times'
arr.length.times {|i| print "#{arr[i]}\t"}

puts
puts '10 downto 2'
10.downto(2) {|i| print "#{i}\t"}

puts
puts '10 upto 14'
10.upto(14) {|i| print "#{i}\t"}

puts
puts 'each_index'
arr.each_index {|i| puts "arr[#{i}] = #{arr[i]}"}

def powers_of_2(x)
    for i in 0..x
        yield 2**i
    end
end

puts 'powers of 2 generator'
powers_of_2(6) {|i| puts i}
