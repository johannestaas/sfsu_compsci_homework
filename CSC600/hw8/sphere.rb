
class Sphere
    
    def initialize(rad)
        @radius = Float(rad)
    end

    def area()
        4.0*Math::PI*(@radius**2)
    end

    def volume()
        4.0/3.0*Math::PI*(@radius**3)
    end

end

class Ball < Sphere
    
    def initialize(rad, color)
        super(rad)
        @color = color
    end

end

class MyBall < Ball
    
    def initialize(rad, color, owner)
        super(rad, color)
        @owner = owner
    end

    def show()
        puts 'MyBall instance'
        puts "Radius: #{@radius}"
        puts "Area: #{area}"
        puts "Volume: #{volume}"
        puts "Color: #{@color}"
        puts "Owner: #{@owner}"
        puts
    end

end

b = MyBall.new(5, :blue, :johan)
b.show

b = MyBall.new(2, :red, :michelle)
b.show
