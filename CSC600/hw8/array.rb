
class Array
    def limited?(amin, amax)
        for i in self
            if i<amin
                return false
            elsif i>amax
                return false
            end
        end
        return true
    end

    def sorted?()
        ret = 0
        for i in 1...self.length
            if self[i] < self[i-1]
                if ret == 1
                    return 0
                end
                ret = -1
            elsif self[i] > self[i-1]
                if ret == -1
                    return 0
                end
                ret = 1
            else
                next
            end
        end
        if ret == 0
            return 1
        end
        return ret
    end
end

arr = [1,2,3,2,1]
arr.limited?(0,4)
arr.limited?(-1,3)
arr.limited?(1,2)
arr.sorted?

arr = [1,2,3,4,8]
arr.sorted?

arr = [4,3,3,2,1,-1]
arr.sorted?

arr = [1,1,1,1,1,1]
arr.sorted?
