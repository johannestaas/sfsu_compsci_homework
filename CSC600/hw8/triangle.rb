
class Triangle
    attr_accessor :a,:b,:c
    
    def initialize(*args)
        if args.length == 3
            @a,@b,@c = args
        elsif args.length == 0
            @a=@b=@c=0
        else
            raise ArgumentError, 
            "unexpected number of arguments to triangle initializer"
        end
    end

    def triangle?()
        @a+@b>@c and @a+@c>@b and @b+@c>@a
    end

    def equilateral?()
        triangle? and @a == @b and @b == @c
    end

    def isoceles?()
        triangle? and (@a == @b or @a == @c or @b == @c)
    end

    def scalene?()
        triangle? and not isoceles?
    end

    def right?()
        triangle? and 
            (@a**2 + @b**2 == @c**2 or
             @a**2 + @c**2 == @b**2 or
             @b**2 + @c**2 == @a**2)
    end

    def test()
        if not triangle?
            :not_triangle
        elsif right?
            :right
        elsif equilateral?
            :equilateral
        elsif isoceles?
            :isoceles
        else
            :scalene
        end
    end

    def perimeter()
        if triangle?
            @a + @b + @c
        end
        # else nil
    end

    def area()
        if triangle?
            p = Float(perimeter)/2
            Math::sqrt(Float(p*(p-@a)*(p-@b)*(p-@c)))
        end
        # else nil
    end
end

