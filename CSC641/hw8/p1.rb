#!/usr/bin/env ruby

class Resource
    attr_accessor :type, :m, :d
    def initialize(res_type, num_of_res, demand)
        # eg. disk, cpu, etc.
        @type = res_type
        @m = num_of_res
        # (milliseconds)
        @d = demand
    end
end

class MVA
    attr_accessor :x, :q_j, :r
    def initialize(input_data)
        @input_data = input_data
    end
    def mva(jobs)
        # initializae q_j[0]
        @q_j = [[]]
        @input_data.length.times do
            @q_j[0] += [0]
        end
        jobs.times do |i|
            resi_t = []
            @input_data.each_index do |id|
                data = @input_data[id]
                resi_t += [data.d*(1+@q_j[i][id])]
            end
            @r = 0.0
            @input_data.each_index do |id|
                data = @input_data[id]
                @r += data.m*resi_t[id]
            end
            @x = Float(i)/@r
            @q_j[i+1] = []
            @input_data.each_index do |id|
                data = @input_data[id]
                @q_j[i+1] += [@x*resi_t[id]]
            end
        end
        return {
            :throughput => @x,
            :queue_length => @q_j[jobs],
            :response_time => @r,
        }
    end
end

input_data_p1a = [
    Resource.new(:cpu, 1, 0.003),
    Resource.new(:disk, 1, 0.010),
    Resource.new(:disk, 1, 0.010),
    Resource.new(:disk, 1, 0.010),
    Resource.new(:disk, 1, 0.010),
]

p1a = MVA.new(input_data_p1a)
p1a.mva(5)

res1 = p1a.mva(8)

input_data_p1b = [
    Resource.new(:cpu, 1, 0.003),
    Resource.new(:disk, 1, 0.040*0.4),
    Resource.new(:disk, 1, 0.040*0.3),
    Resource.new(:disk, 1, 0.040*0.2),
    Resource.new(:disk, 1, 0.040*0.1),
]

p1b = MVA.new(input_data_p1b)
res2 = p1b.mva(8)

puts 'a)'
puts 'X = ' + res1[:throughput].to_s + ' seconds'
puts 'R = ' + res1[:response_time].to_s + ' seconds'
input_data_p1a.each_index do |i|
    res = input_data_p1a[i]
    puts "#{res.type}#{i} queue: #{res1[:queue_length][i]}"
    puts "#{res.type}#{i} util: #{res1[:throughput]*res.d}"
end

puts
puts 'b)'
puts 'X = ' + res2[:throughput].to_s + ' seconds'
puts 'R = ' + res2[:response_time].to_s + ' seconds'
input_data_p1b.each_index do |i|
    res = input_data_p1b[i]
    puts "#{res.type}#{i} queue: #{res2[:queue_length][i]}"
    puts "#{res.type}#{i} util: #{res2[:throughput]*res.d}"
end
