#!/usr/bin/env ruby

class Resource
    attr_accessor :type, :m, :d
    def initialize(res_type, num_of_res, demand)
        # eg. disk, cpu, etc.
        @type = res_type
        @m = num_of_res
        # (milliseconds)
        @d = demand
    end
end

class MVA
    attr_accessor :x, :q_j, :r, :z
    def initialize(input_data, z)
        @input_data = input_data
        @z = z
    end
    def mva(jobs)
        # initializae q_j[0]
        @q_j = [[]]
        @input_data.length.times do
            @q_j[0] += [0]
        end
        jobs.times do |i|
            resi_t = []
            @input_data.each_index do |id|
                data = @input_data[id]
                resi_t += [data.d*(1+@q_j[i][id])]
            end
            @r = 0.0
            @input_data.each_index do |id|
                data = @input_data[id]
                @r += data.m*resi_t[id]
            end
            @x = Float(i)/(@z+@r)
            @q_j[i+1] = []
            @input_data.each_index do |id|
                data = @input_data[id]
                @q_j[i+1] += [@x*resi_t[id]]
            end
        end
        return {
            :throughput => @x,
            :queue_length => @q_j[jobs],
            :response_time => @r,
        }
    end
end

input_data_p1a = [
    Resource.new(:cpu, 1, 0.003),
    Resource.new(:disk, 1, 0.010),
    Resource.new(:disk, 1, 0.010),
    Resource.new(:disk, 1, 0.010),
    Resource.new(:disk, 1, 0.010),
]


input_data_p1b = [
    Resource.new(:cpu, 1, 0.003),
    Resource.new(:disk, 1, 0.040*0.4),
    Resource.new(:disk, 1, 0.040*0.3),
    Resource.new(:disk, 1, 0.040*0.2),
    Resource.new(:disk, 1, 0.040*0.1),
]


p1a = MVA.new(input_data_p1a, 5)
File.open('3a.csv', 'w') do |f|
    1008.times do |j|
        res1 = p1a.mva(j+1)
        x = res1[:throughput]
        r = res1[:response_time]

        f.write("#{x}, #{r}")
        input_data_p1a.each_index do |i|
            res = input_data_p1a[i]
            f.write(",#{x*res.d}")
        end
        f.write("\n")
    end
end

p1b = MVA.new(input_data_p1b, 5)
File.open('3b.csv', 'w') do |f|
    730.times do |j|
        res1 = p1b.mva(j+1)
        x = res1[:throughput]
        r = res1[:response_time]

        f.write("#{x}, #{r}")
        input_data_p1b.each_index do |i|
            res = input_data_p1b[i]
            f.write(",#{x*res.d}")
        end
        f.write("\n")
    end
end
