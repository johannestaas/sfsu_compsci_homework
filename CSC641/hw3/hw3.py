#/usr/bin/env python
''' 
CSC641: Homework 3
Johan Nestaas
Quantum Atlas III disk simulator
'''

import random
# bisect is the popular module with a sorted list implementation
import bisect
import math

# sqrt-ish part
def f1(dx, t, c, r):
    return t + c*((dx-1)**r)

# linear-ish part
def f2(dx, c, r, x, t):
    ret = c*r*(dx-x)/((x-1)**(1-r))
    ret += t
    ret += c*((x-1)**r)
    return ret

# theoretical seek time
def f3(dx, tmax, xmax):
    return tmax * math.sqrt(abs(dx/xmax))

class Disk(object):
    ''' disk class with a disk queue "dq" '''
    # these initial settings are for the QuantumAtlasIII
    xmax = 8057 # cyl
    C = 9.1 # GB
    N = 7200 # RPM
    x = 1686
    t = 1.5455 # ms
    c = 0.3197 # ms
    r = 0.3868 
    # approximate tmax with the function with distance set to xmax
    tmax = f1(xmax, t, c, r)
        
    def __init__(self, qsize=10):
        ''' initialize disk queue and pos '''
        self.dq = sorted([Disk.random_cyl() for x in xrange(qsize)])
        self.pos = Disk.random_cyl()
        self.seek_dist = 0
        self.seek_time = 0
        self.theoretical_seek_time = 0
        self.seek_count = 0
    
    def seek(self):
        ''' seeks by SSTF to the closest point in the queue '''
        index = bisect.bisect(self.dq, self.pos)

        if len(self.dq) == 1:
            dist = abs(self.pos - self.dq[0])
            self.pos = self.dq[0]
            self.dq[0] = Disk.random_cyl()
        else:
            try:
                left_dist = self.pos - self.dq[index-1]
            except IndexError:
                # nothing to the left
                # farther than it can be, to skip it
                left_dist = self.__class__.xmax + 1
            try:
                right_dist = self.dq[index] - self.pos
            except IndexError:
                # nothing to the right
                right_dist = self.__class__.xmax + 1

            if left_dist < right_dist:
                dist = left_dist
                self.pos = self.dq[index-1]
                del self.dq[index-1]
            else:
                dist = right_dist
                self.pos = self.dq[index]
                del self.dq[index]
            bisect.insort(self.dq, Disk.random_cyl())
        self.seek_dist += dist
        self.seek_time += Disk.T_seek(dist)
        self.theoretical_seek_time += Disk.theoretical_T_seek(dist)
        self.seek_count += 1
    
    def avg_seek_time(self):
        try:
            return self.seek_time / float(self.seek_count)
        except:
            return 0
    
    def avg_theoretical_seek_time(self):
        ''' theoretical seek time '''
        try:
            return self.theoretical_seek_time / float(self.seek_count)
        except:
            return 0

    def avg_seek_dist(self):
        try:
            return self.seek_dist / float(self.seek_count)
        except:
            return 0

    def output(self):
        print('Queue Length: %d' % len(self.dq))
        print('Avg Seek Distance (cyl):\t%f' % self.avg_seek_dist())
        print('Avg Seek Time (ms):\t\t%f' % self.avg_seek_time())

    @classmethod
    def random_cyl(cls):
        return int(random.uniform(0, cls.xmax))

    @classmethod
    def T_seek(cls, dist):
        if 1 <= dist and dist <= cls.x:
            return f1(dist, cls.t, cls.c, cls.r)
        else:
            return f2(dist, cls.c, cls.r, cls.x, cls.t)

    @classmethod
    def theoretical_T_seek(cls, dist):
        return f3(dist, cls.tmax, cls.xmax)
        
if __name__ == '__main__':
    out = open('diskq.csv', 'w')
    out.write('Disk Queue Length, Average Seek Distance, Average Seek Time\n')
    for qsize in xrange(1, 21):
        d = Disk(qsize)
        for i in xrange(100000):
            d.seek()
        d.output()
        out.write('%d,%f,%f,%f\n'
            % (len(d.dq), 
               d.avg_seek_dist(), 
               d.avg_seek_time(),
               d.avg_theoretical_seek_time()))
    out.close()
