// Johan Nestaas
// CSC 641
// Computer Performance Evaluation
// Homework 1

// integer lists
// * implements integer sorting
//

#include <stdexcept>
#include <iostream>

class UnsortedList {
public:
    UnsortedList(unsigned int);
    ~UnsortedList();
    void slow_sort();
    void output();
private:
    unsigned int size;
    int* head;
};

UnsortedList::UnsortedList(unsigned int s)
{
    size = s;
    if(!s) 
        throw std::runtime_error("Can't take zero as a list size!");
    head = new int[size];
    for(int i=0; i<size; i++)
        head[i] = 100 - (i % 100);
}

UnsortedList::~UnsortedList()
{
    delete[] head;
    head = NULL;
}

void UnsortedList::slow_sort() 
{
    for(int i=0; i<size-1; i++) {
        for(int j=i+1; j<size; j++) {
            if(head[i] > head[j]) {
                int tmp = head[i];
                head[i] = head[j];
                head[j] = tmp;
            }
        }
    }
}

void UnsortedList::output()
{
    using namespace std;
    for(int i=0; i<size; i++)
        cout << head[i] << " ";
    cout << endl;
}

