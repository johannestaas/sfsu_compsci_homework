// Johan Nestaas
// CSC 641
// Computer Performance Evaluation
// Homework 1

#include "matrix.h"
#include "unsortedlist.h"
#include <iostream>
#include <time.h>

#define MAT_SIZE 850
#define LIST_SIZE 65000

using namespace std;

int main( int argc, const char* argv[] )
{
    
    double ticks = 0;
    double d_ticks = 0;
    double i_ticks = 0;

    cout << "Calibrating with INC operation.\n";
    time_t current_time;
    time_t calibrate = time(NULL);
    while(time(NULL) == calibrate);
    calibrate = time(NULL);
    while(time(NULL) == calibrate)
        ticks++;
    cout << "Calibrated with " << ticks << " ticks per second.\n";

    time_t double_time = time(NULL);
    for(int i=0; i<10; i++)
    {
        time_t duration = time(NULL);
        Matrix *m = new Matrix(MAT_SIZE);
        m->set_sample();
        Matrix *m2 = m->inverted();
        duration = time(NULL) - duration;
        delete m, m2;
    }
    double_time = time(NULL) - double_time;
    current_time = time(NULL);
    while(time(NULL) == current_time)
        d_ticks++;
    d_ticks = ticks - d_ticks;
    double Tf = (double)double_time + (d_ticks/ticks);
    cout << "Finished double matrix inversions in "
         << Tf << " seconds ("
         << MAT_SIZE << " rows and columns, 10 matrices)." 
         << endl;

    time_t int_time = time(NULL);
    for(int i=0; i<10; i++)
    {
        time_t duration = time(NULL);
        UnsortedList *list = new UnsortedList(LIST_SIZE);
        list->slow_sort();
        duration = time(NULL) - duration;
        delete list;
    }
    int_time = time(NULL) - int_time;
    current_time = time(NULL);
    while(time(NULL) == current_time)
        i_ticks++;
    i_ticks = ticks - i_ticks;
    double Ti = (double)int_time + (i_ticks/ticks);
    cout << "Finished int sorting in "
         << Ti << " seconds ("
         << LIST_SIZE << " integers, 10 lists)"
         << endl;

    float Vf = 60.0 / Tf;
    float Vi = 60.0 / Ti;
    float Vproc = 2/((1/Vf) + (1/Vi));

    cout << endl;
    cout << "STATISTICS\n";
    cout << "**********\n";
    cout << "Tf:    \t" << Tf << endl;
    cout << "Ti:    \t" << Ti << endl;
    cout << "Vf:    \t" << Vf << endl;
    cout << "Vi:    \t" << Vi << endl;
    cout << endl;
    cout << "Vproc: \t" << Vproc << endl;
    return 0;
}

