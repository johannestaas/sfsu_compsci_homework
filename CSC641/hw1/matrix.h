// Johan Nestaas
// CSC 641
// Computer Performance Evaluation
// Homework 1

// A two dimensional square matrix of doubles
// * Dynamically allocates size at instanciation
// * implements matrix inversion
//

#include <stdexcept>
#include <iostream>

class Matrix {
public:
    Matrix(unsigned int);
    ~Matrix();
    void kill_matrix();
    void output();
    Matrix* inverted();
    double** get_matrix();
    void set_identity();
    void set_sample();
    void row_add(unsigned int, unsigned int, double);
    void row_div(unsigned int, double);
    double get(unsigned int, unsigned int);
private:
    unsigned int size;
    double **matrix;
};

Matrix::Matrix(unsigned int s)
{
    size = s;
    if(!s) 
        throw std::runtime_error("Can't take zero as a row or column!");
    matrix = new double*[s];
    for(int row=0; row<s; row++)
        matrix[row] = new double[s];
}

Matrix::~Matrix()
{
    kill_matrix();
}

double** Matrix::get_matrix()
{
    return matrix;
}

void Matrix::kill_matrix()
{
    if(!size) 
        throw std::runtime_error("No size of matrix!"
                                 "(something really bad happened)");
    for(int row=0; row<size; row++)
    {
        delete[] matrix[row];
        for(int col=0; col<size; col++) matrix[row] = NULL;
    }
    delete[] matrix;
    matrix = NULL;
}

void Matrix::set_identity() 
{
    if(!size) 
        throw std::runtime_error("No size of matrix for destructor!"
                                 "(something really bad happened)");
    for(int row=0; row<size; row++) 
    {
        for(int col=0; col<size; col++) 
        {
            if(row == col) 
                matrix[row][col] = 1.0L;
            else
                matrix[row][col] = 0.0L;
        }
    }
}

void Matrix::set_sample()
{
    if(!size) 
        throw std::runtime_error("No size of matrix for destructor!"
                                 "(something really bad happened)");
    for(int row=0; row<size; row++) 
    {
        for(int col=0; col<size; col++) 
        {
            if(row == col) 
                matrix[row][col] = 2.0001L;
            else
                matrix[row][col] = 1.0001L;
        }
    }
}

double Matrix::get(unsigned int row, unsigned int col)
{
    if(row >= size || col >= size)
        throw std::runtime_error("Attempt to access invalid index!");
    return matrix[row][col];
}

// if I wanted to be crazy, I could implement some SIMD:
// http://stackoverflow.com/questions/8389648/how-to-achieve-4-flops-per-cycle
// http://en.wikipedia.org/wiki/SIMD
void Matrix::row_add(unsigned int dest_row, unsigned int src_row, double factor)
{
    if(dest_row >= size || src_row >= size)
        throw std::runtime_error("Attempt to access invalid index!");
    double *row1 = matrix[dest_row];
    double *row2 = matrix[src_row];
    for(int col=0; col<size; col++)
        row1[col] += (row2[col] * factor);
}

void Matrix::row_div(unsigned int row, double div)
{
    if(row >= size)
        throw std::runtime_error("Attempt to access invalid index!");
    if(div == 1) return;
    double *dest_row = matrix[row];
    for(int col=0; col<size; col++)
        dest_row[col] /= div;
}

Matrix* Matrix::inverted()
{
    Matrix* id = new Matrix(size);
    id->set_identity();
    for(int col=0; col<size; col++) 
    {
        double one = matrix[col][col];
        row_div(col, one);
        id->row_div(col, one);
        for(int row=0; row<size; row++) 
        {
            double val = matrix[row][col];
            if(row == col) continue;
            row_add(row, col, -1*val);
            id->row_add(row, col, -1*val);
        }
    }
    return id;
}

void Matrix::output()
{
    using namespace std;
    for(int row=0; row<size; row++)
    {
        for(int col=0; col<size; col++)
        {
            double val = matrix[row][col];
            cout << val << "\t";
        }
        cout << endl;
    }
    cout << endl;
}

