#/usr/bin/env python
''' 
CSC641: Homework 3
(MODIFIED FOR MIDTERM REVIEW)
Johan Nestaas
'''

import random
# bisect is the popular module with a sorted list implementation
import bisect
import math

class Disk(object):
    ''' disk class with a disk queue "dq" '''
        
    def __init__(self, qsize=10):
        ''' initialize disk queue and pos '''
        self.dq = sorted([Disk.random_cyl() for x in xrange(qsize)])
        self.pos = Disk.random_cyl()
        self.seek_dist = 0
        self.seek_time = 0
        self.theoretical_seek_time = 0
        self.seek_count = 0
    
    def seek(self):
        ''' seeks by SSTF to the closest point in the queue '''
        index = bisect.bisect(self.dq, self.pos)
        dist = abs(self.pos - self.dq[0])
        self.pos = self.dq[0]
        self.dq[0] = Disk.random_cyl()
        self.seek_dist += dist
        self.seek_time += self.T_seek(dist)
        self.seek_count += 1
    
    def avg_seek_time(self):
        try:
            return self.seek_time / float(self.seek_count)
        except:
            return 0

    def avg_seek_dist(self):
        try:
            return self.seek_dist / float(self.seek_count)
        except:
            return 0

    def output(self):
        print('Queue Length: %d' % len(self.dq))
        print('Avg Seek Distance (cyl):\t%f' % self.avg_seek_dist())
        print('Avg Seek Time (ms):\t\t%f' % self.avg_seek_time())

    @staticmethod
    def random_cyl():
        return int(random.uniform(0, 300))

    def T_seek(self, dist):
        return math.sqrt(dist)
        
if __name__ == '__main__':
    d = Disk(1)
    for i in xrange(100000):
        d.seek()
    d.output()
