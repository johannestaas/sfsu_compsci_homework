#include <stdlib.h>
#include <math.h>
#include <time.h>

double rand01()
{
    return rand() / (double)(RAND_MAX);
}

int main() 
{
    double x,y,n;
    int i;
    srand(time(NULL));
    n = 0.0;
    for(i=0;i<10000000;i++)
    {
        x = rand01();
        y = rand01() * exp(1.0);
        if(y<exp(x))
            n++;
    }
    y = ((n/10000000.0) * exp(1.0)) + 1.0;
    printf("e: %f\n", y);
    return 0;
}
