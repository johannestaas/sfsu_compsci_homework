import sys

def lehmer(n, a, c):
    return (a*n + c) % 256

a = 1

while True:
    a += 1
    n = 0
    for c in xrange(256):
        nums = []
        for tryall in xrange(256):
            n = lehmer(n, a, c)
            if n in nums:
                break
            nums += [n]
        else:
            assert len(nums) == 256, 'not full length!!!'
            print('found a good sequence with a=%d and c=%d' % (a,c))
            print('sequence: ')
            print(nums)
            sys.exit()
