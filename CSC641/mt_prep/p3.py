import random

def r1234():
    r = random.random()
    if r<0.1:
        return 1
    if r<0.3:
        return 2
    if r<0.6:
        return 3
    return 4

def rng():
    return r1234() * 10 + r1234()
