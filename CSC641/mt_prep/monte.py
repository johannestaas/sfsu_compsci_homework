#!/usr/bin/env python

import math
import random

def ex(x):
    return math.e**x

n=0
N=0

for i in xrange(1000000):
    x = random.uniform(0,1)
    y = random.uniform(0,1)*math.e
    if y<ex(x):
        n+=1
    N+=1

print((float(n)/float(N) * math.e) + 1.0)

