#!/usr/bin/env lua

math.randomseed(os.time())

SIMS = 10000000

-- generate exponentially distributed random numbers
rand_expo = function(mean)
    return mean*(-1*(math.log(math.random())))
end

-- generate random numbers with mean
rand_unif = function(mean)
    return (mean*math.random()) + 10
end

-- just so I can easily swap out functions for the simulator
const = function(const)
    return const
end

-- service request
Request = {}
Request.__index = Request
Request.new = function(service_timer, interarrival_timer)
    r = {}
    setmetatable(r, Request)
    r.response_time = 0
    r.service_timer = service_timer
    r.interarrival_timer = interarrival_timer
    return r
end

Stat = {}
Stat.__index = Stat
Stat.new = function(name)
    s = {}
    setmetatable(s, Stat)
    s.total = 0
    s.name = name
    return s
end

function Stat:inc(x)
    self.total = self.total + x
    table.insert(self, x)
end

function Stat:avg()
    return self.total / #self
end

function Stat:sigma()
    sig_tot = 0
    avg = self:avg()
    for k,v in ipairs(self) do
        sig_tot = sig_tot + math.pow(avg-v,2)
    end
    return math.sqrt(sig_tot/(#self-1))
end 

function Stat:out(div)
    print('Average '..self.name..': \t'..self:avg()/div)
    print('Stddev of '..self.name..': \t'..self:sigma()/div)
end

Record = {}
Record.__index = Record
Record.new = function()
    r = {}
    setmetatable(r, Record)
    r.interarrival_times = Stat.new('Interarrival Time')
    r.service_times = Stat.new('Service Time')
    r.response_times = Stat.new('Response Time')
    r.queue_lengths = Stat.new('Queue Length')
    r.avg_util = Stat.new('Server Utilization')
    return r
end

function Record:out()
    self.interarrival_times:out(10)
    self.service_times:out(10)
    self.response_times:out(10)
    self.queue_lengths:out(1)
    self.avg_util:out(1)
end

function Record:out_csv(title)
    local file = io.open('stats.csv', 'a')
    file:write(title..'\n')
    file:write('utilization,queue length,response time\n')
    file:write(self.avg_util:avg(),',',
               self.queue_lengths:avg(),',',
               self.response_times:avg()/10,'\n')
    file:close()
end


-- title = title for printing and outputting to file
-- ia = interarrival time
-- s = service time
simulate = function(title, ia_func, ia_mean, s_func, s_mean)
    rec = Record.new()
    -- numeric keys are service requests, fifo
    -- "server" is time until current customer is finished, or nil for empty
    -- "future" is the next request to enter the queue
    queue = {server=nil,future=nil}
    -- the next one to join the queue
    ia = ia_func(ia_mean)
    st = s_func(s_mean)
    rec.interarrival_times:inc(ia)
    rec.service_times:inc(st)
    queue.future = Request.new(st, ia)
    
    for turn=1,SIMS do
        fut = queue.future
        fut.interarrival_timer = fut.interarrival_timer - 1
        if fut.interarrival_timer <= 0 then
            table.insert(queue, fut)
            ia = ia_func(ia_mean)
            st = s_func(s_mean)
            rec.interarrival_times:inc(ia)
            rec.service_times:inc(st)
            queue.future = Request.new(st, ia)
        end
        if not queue.server then
            if #queue > 0 then
                queue.server = table.remove(queue, 1)
            end
        end
        if queue.server then
            req = queue.server
            req.service_timer = req.service_timer - 1
            req.response_time = req.response_time + 1
            rec.avg_util:inc(1)
            if req.service_timer <= 0 then
                rec.response_times:inc(req.response_time)
                queue.server=nil
            end
            rec.queue_lengths:inc(#queue + 1)
        else
            rec.queue_lengths:inc(#queue)
            rec.avg_util:inc(0)
        end
        for _,req in ipairs(queue) do
            req.response_time = req.response_time + 1
        end
    end
    print(title)
    rec:out()
    rec:out_csv(title)
end

test = function()
    simulate('D/D/1', const, 20, const, 10)
    simulate('D/M/1', const, 20, rand_expo, 10)
    simulate('D/G/1', const, 20, rand_unif, 10)
    simulate('M/D/1', rand_expo, 20, const, 10)
    simulate('M/M/1', rand_expo, 20, rand_expo, 10)
    simulate('M/G/1', rand_expo, 20, rand_unif, 10)
    simulate('G/D/1', rand_unif, 20, const, 10)
    simulate('G/M/1', rand_unif, 20, rand_expo, 10)
    simulate('G/G/1', rand_unif, 20, rand_unif, 10)
end

test()
