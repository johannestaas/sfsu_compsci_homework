// Johan Nestaas
// CSC 641
// Computer Performance Evaluation
// Homework 2

#include <iostream>
#include <fstream>

#include "jtime.h"
#include "jrand.h"

#define FILESIZE 50000000
#define BUFFER_SIZE 512
#define SEQ_READS 10
#define RAN_READS 80

using namespace std;

int create_file(char* filename)
{
    ofstream fs;
    char buf[BUFFER_SIZE];
    for(int i=0; i<BUFFER_SIZE; i++) {
        buf[i] = 'A';
    }
    fs.open(filename);
    for(int i=0; i<FILESIZE; i+=BUFFER_SIZE)
        fs << buf;
    fs.close();
    return 0;
}

int DSEQ(char* filename)
{
    char buf[BUFFER_SIZE];
    char last;
    create_file(filename);
    ifstream fs;
    for(int i=0; i<SEQ_READS; i++) 
    {
        fs.open(filename);
        while(!(fs.eof()))
        {
            fs.read(buf, sizeof(buf));
            last = buf[0];
        }
        fs.close();
    }
    return last;
}

int DRAN(char* filename)
{
    JRand jr = JRand();
    char buf[BUFFER_SIZE];
    char last;
    create_file(filename);
    ifstream fs;
    fs.open(filename);
    // reads random chunks many times
    for(int i=0; i<RAN_READS; i++)
    {
        float seek = jr.rand() * (float)FILESIZE;
        fs.seekg(seek, ios::beg);
        fs.read(buf, sizeof(buf));
        last = buf[0];
    }
    fs.close();
    return last;
}

int main(int argc, char* argv[])
{
    double seq_time, ran_time;
    char* filename = "bench_temp.testfile";

    if (argc == 2) filename = argv[1];
    
    if (argc > 2) 
    {
        cerr << "Usage: bench_disk [filename]\n";
        return -1;
    }

    JTime *jt = new JTime();
    cout << "Calibrated with " 
         << jt->get_calibration()
         << " INCs per second.\n";
    
    cout << "Starting tests.\n";

    jt->check();
    for(int i=0; i<20; i++)
        DSEQ(filename);
    seq_time = jt->check();
    cout << "Tseq:\t" << seq_time << endl;

    jt->check();
    for(int i=0; i<10; i++)
        DRAN(filename);
    ran_time = jt->check();
    cout << "Tran:\t" << ran_time << endl;

    double Vseq, Vran, Vdisk;
    Vseq = 60 / seq_time;
    Vran = 60 / ran_time;
    Vdisk = 2/((1/Vseq)+(1/Vran));

    cout << "Vseq:\t" << Vseq << endl;
    cout << "Vran:\t" << Vran << endl;
    cout << endl;
    cout << "Vdisk:\t" << Vdisk << endl;

    delete jt;

    return 0;
}

