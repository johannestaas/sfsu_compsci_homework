// Johan Nestaas
// CSC 641
// Computer Performance Evaluation
// Timing library for my own uses

#include <time.h>

#define CALIBRATIONS 5
class JTime 
{
public:
    // creates JTime instance and calibrates
    JTime();
    ~JTime();
    // returns seconds since last check
    double check();
    double get_calibration() { return ticks_per_sec; };
private:
    // the last time the JTime was checked
    time_t last;
    double ticks_per_sec;
};

// Calibrates with INC operation <CALIBRATIONS> times
JTime::JTime()
{
    double ticks[CALIBRATIONS];
    time_t calibrate = time(NULL);
    ticks_per_sec = 0.;
    for(int i=0; i<CALIBRATIONS; i++)
    {
        ticks[i] = 0.;
        // wait for last second to flip
        while(time(NULL) == calibrate);
        calibrate = time(NULL);
        while(time(NULL) == calibrate)
            ticks[i]++;
        ticks_per_sec += ticks[i];
    }
    // Take the average of all the calibrations.
    ticks_per_sec /= CALIBRATIONS;
    last = time(NULL);
}

JTime::~JTime() {}

// this will actually take a second or two,
// because it flips to the next second.
double JTime::check()
{
    double ret, ticks;
    time_t now = time(NULL);
    ticks = 0.;
    while(time(NULL) == now)
        ticks++;
    ticks = (ticks_per_sec - ticks) / ticks_per_sec;
    if (ticks < 0)
        ticks = 0;
    ret = now - last + ticks;
    last = time(NULL) + 1;
    // wait the last portion of the second
    // for a fresh start
    while(time(NULL) < last);
    return ret;
}

