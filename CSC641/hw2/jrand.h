// Johan Nestaas
// CSC 641
// Computer Performance Evaluation
// Random number generator

#include <time.h>
#include <iostream>

#define BASE 65536

// my pseudo-random number generator
// using multiply-with-carry because it's very simple
// 
// x = (a*x + c) % b
// http://en.wikipedia.org/wiki/Multiply-with-carry


class JRand
{
public:
    JRand();
    ~JRand();
    // get a random byte
    char byte();
    // get a random ascii char
    char ascii();
    // get random [0-1)
    float rand();
private:
    void next();
    int multiplier;
    int carry;
    int x;
};

// initialize with time ticks as seed
JRand::JRand()
{
    // init multiplier to 0
    multiplier = 0;
    // start carry as the current time
    carry = time(NULL);
    while((carry == time(NULL)) || !multiplier)
        multiplier++;
    // wherever we were in between seconds
    // (as number of INCs possible)
    // will be our random multiplier

    // carry will be randomized a bit
    carry = (carry >> 16) * (multiplier >> 16);

    // randomize x a little
    x = (multiplier << 16) | (carry >> 16);

    // this should be sufficient randomization
    // for benchmarking purposes, but probably
    // not for cryptography.
}

JRand::~JRand() {}

void JRand::next()
{
    int x2 = (multiplier*x + carry) % BASE;
    carry = (multiplier*x + carry) / BASE;
    x = x2;
    if(x<0)x*=-1;
}

char JRand::byte()
{
    next();
    return (char)(x % 256);
}

char JRand::ascii()
{
    next();
    return (char)((x % 0x5e) + 0x20);
}

float JRand::rand()
{
    next();
    float ret = (float)x;
    ret = ret / BASE;
    return ret;
}
